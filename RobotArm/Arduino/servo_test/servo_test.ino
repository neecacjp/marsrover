#include <Servo.h>

//サーボモーターの使用宣言
Servo hand_servo;
Servo wrist_servo;
//Servo neck_servo;


void setup() {
  //サーボモーターの使用ピン設定
  hand_servo.attach(11);
  wrist_servo.attach(12);
  //neck_servo.attach(13);
  
  // サーボモーターを初期値に設定
  hand_servo.write(140);
  wrist_servo.write(90);
  //neck_servo.write(60);

  //delay(1000); // 少し待つ
  
}

void loop() {
  // 物を持ち上げる
  wrist_servo.write(90+30);
  delay(3000);
  hand_servo.write(140+30);
  delay(3000);

  // 手を離す
  wrist_servo.write(90-30);
  delay(3000);  
  hand_servo.write(140-30);
  delay(3000);
}
