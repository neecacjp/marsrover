#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys, json
import MotorControl

data = json.loads(sys.stdin.readline())

def main():
	MotorControl.gpioSetup()

	if data[1] == '前進':
		response = {"result" : 'rover前進プログラム実行'}
		MotorControl.advance():
	elif data[1] == '後退':
		response = {"result" : 'rover後退プログラム実行'}
		MotorControl.back():
	elif data[1] == '停止':
		response = {"result" : 'rover停止プログラムの実行'}
		MotorControl.stop():
	else:
		response = {"result" : 'error'}

	#print(result)
	print(json.dumps(response))

if __name__ == "__main__":
    main()

