var superagent = require('superagent');
var firebase = require("firebase");
const os = require("os")
//const query = require("./query.js");
//const execute = require("./execute.js");

// firebase config
var config = {
    apiKey: "AIzaSyDF_XNXNWiHbDhRt_AEn2nXGVtnrBwzvAg",
    authDomain: "marsproject2.firebaseapp.com",
    databaseURL: "https://marsproject2.firebaseio.com",
    projectId: "marsproject2",
    storageBucket: "marsproject2.appspot.com",
    messagingSenderId: "542144284575"
  };
firebase.initializeApp(config);

//database更新時
const path = "googlehome";
const key = "word";
const db = firebase.database();

db.ref(path).on("value", function(changedSnapshot) {
  //値取得
  console.log("[Trigger] DB changed")
  let value = changedSnapshot.child(key).val();
  
  if (value) {
    //コマンド生成

	//まずpython-shellモジュールを読み込む
	var { PythonShell } = require('python-shell');

	//json形式で別ファイルのpython(rover_control.py)にデータを渡すことを前提にオブジェクト作成
	var shell = new PythonShell('./python/rover_control.py', {
    	mode: 'json',
	});
	
	const parsedData = value.split(" ");
	const k = parsedData[0];
	const v = parsedData[1];

	//jsonデータ作成
	var json = new Array();
		json[0] = k;
		json[1] = v;
	console.log(json[0]);
    console.log(json[1]);

	// rover_control.pyにJSONを送信
	shell.send(json);

	// script.pyからの返事を待機
	shell.on('message', data => {
    	// データを表示 
   		 console.log(data.result);
	});

	// 入力を終了
	shell.end();

      //firebase clear
      db.ref(path).set({[key]: ""}); 
  }
});

console.log("loaded");

db.ref("/connected").set({
  hostname: os.hostname(),
  time: new Date().toString(),
  user:  os.userInfo(),
  platform: {
    cpu: os.cpus(),
    mem: {
      total: os.totalmem(),
      free: os.freemem()
    },
    os: os.platform(),
    arch: os.arch(),
    release: os.release(),
  }
})

